#!/usr/bin/env python3
# A script to find best match between simulation results and experimental data
# via a genetic algorithm.
# Maud Formanek, 2020, Francois Nedelec October 2021, Dec 2021, Jan--March 2022
# Copyright Sainsbury Laboratory, Cambridge University, UK


"""
    Perform a parameter search using a genetic algorithm. Each parameter set
    represents an individual and its fitness is defined by running simulations,
    whose results are analysed on the fly and compared to the 'target' data.
 
    This will use 'preconfig.py' to vary parameters in a template file.
    Copy `preconfig.py' into the directory (see `preconfig.py' documentation)
    
    It relies on a module called 'arena' to evaluate the outcome of simulations,
    and to define executable and template files. Check for 'arena.' in this file.
    You can reprogram the genetic algorithm by providing a new version of 'arena.py'
    with the same methods, but performing a different calculation of fitness.
    
Syntax:

    evolve.py [RESTART] [evolve.config]
    
    The RESTART argument can be used to set a generation to initialize from.
    RESTART should be the name of a folder containing 'generation.txt', usually
    a directory created from a previous simulation.
    The parameters are read from `evolve.config' or from the specified file.

M. Formanek, 2020, FJ. Nedelec 2021--2022
"""

# Loading modules
try:
    import os
    import sys
    import re
    # import math
    import random
    # import subprocess
    import resource
    from multiprocessing import Process, Queue
    # from random import sample
    from typing import List
    from genetics import load_genetics
    from genetools import *
    from creature import *
    import preconfig
    #import swangle_lib as arena
    import arena
except ImportError as e:
    sys.stderr.write("Error loading module: %s\n" % str(e))
    sys.exit()

# -------------------------------------------------------------------------------


def save_generation(filename, pop):
    """ save summary of all individuals to text file """
    f = open(filename, 'w', buffering=1)
    for i in pop:
        fit = i.fitness
        seq = i.sequence()
        val = tidy_dictionary(i.values())
        f.write("%s %10.6f %s  %s\n" % (i.path, fit, seq, val))
    f.close()


def load_generation(filename, code) -> List[Creature]:
    """ load a population from text file """
    pop = []
    file = open(filename, 'r')
    i = 0
    for line in file:
        s = line.split()
        if re.match('gen[0-9]{4}', s[0]):
            f = float(s[1])
            g = s[2]
        elif s[0] == 'fitness':  # old format
            f = float(s[1])
            g = s[2]
        elif s[1] == 'fitness':  # older format
            f = float(s[2])
            g = s[4]
        else:
            continue
        pop.append(Creature(code, g, i, f))
        #print("loaded  %s fitness %10.6f"%(g,f))
        i += 1
    file.close()
    return pop

# -------------------------------------------------------------------------------


def bars(val, cnt):
    """ generate a string of size 'cnt' representing val, which should be in [0, 1]"""
    n = min(max(int(cnt*val), 0), cnt)
    return '|'*n + ' '*(cnt-n)


def revive(heaven, bug, resurrect):
    """ check if `bug` appears in current or previous generation"""
    seq = bug.sequence()
    new = bug.path
    if seq in heaven:
        old = heaven[seq]
        if old.split('/')[0] == new.split('/')[0]:
            #print("sequence %s already exists in same generation" %seq)
            while seq in heaven:
                #print(bug.genome, len(bug.genome))
                bug.genome = bug.mutate(0.9/len(bug.genome))
                seq = bug.sequence()
                #print(" mutated -> %s" %seq)
        else:
            if resurrect:
                copy_files(old, new)
                #move_files(old, new)
    heaven[seq] = new


def run_simulation(bug):
    """ Run all config files found in directory 'bug.path' and return fitness """
    # Attention: in multiprocessing, any modification to argument 'bug' is lost!
    # and global variables values are not those in the parent thread!
    cpu = resource.getrusage(resource.RUSAGE_CHILDREN).ru_utime
    cwd = os.getcwd()
    os.chdir(bug.path)
    # get all files ending by '.cym':
    files = []
    with os.scandir() as it:
        for e in it:
            if e.name.endswith('.cym') and e.is_file():
                files.append(e.name)
    #print(bug.path+" -------> "+' '.join(files))
    # run simulations and get textual output:
    all, old = run_many(arena.simex, bug.path, files)
    os.chdir(cwd)
    # fitness from fresh data only:
    fit = arena.calculate_fitness(all, bug.target)
    if old:
        fff = fit
        # add data computed in previous generations:
        all.extend(old)
        cnt = len(old)
        old = arena.calculate_fitness(old, bug.target)
        # set fitness from all available data:
        fit = arena.calculate_fitness(all, bug.target)
        print(bars(fit, 20), end='')
        if 1:
            now = resource.getrusage(resource.RUSAGE_CHILDREN).ru_utime
            print(" cpu %6.1f " % (now-cpu), end='')
        print(" %s  %s  fit %10.4f (%i) %+10.4f  ---> %10.4f" %
              (bug.path, str(bug), old, cnt, fff, fit))
    else:
        #print(" %s  %s  fit %10.4f ---> %10.4f" %(bug.path, str(bug), bug.fitness, fit))
        pass
    return fit


def run_simulation_trace(bug):
    """ Simulate one Creature and return its fitness"""
    # Attention: in multiprocessing, any modification to argument 'bug' is lost!
    # and global variables values are not those in the parent thread!
    try:
        return run_simulation(bug)
    except Exception:
        import traceback
        traceback.print_exc()
        sys.exit()

# -------------------------------------------------------------------------------


def worker(food, poop):
    """
    This is executed by a child processes in multiprocessing mode.
    run simulation taking argument from `feed`, returning results in `poop`
    """
    while True:
        try:
            index, bug = food.get(True, 1)
        except:
            break
        res = run_simulation(bug)
        poop.put((index, res))


def run_parallel(population, njobs):
    """
    Run all 'bug' in the population using multiple threads, and set bug.fitness
    """
    food = Queue()
    poop = Queue()
    # fill queue
    for n, i in enumerate(population):
        food.put_nowait((n, i))
    jobs = []
    # create threads, and start them running 'worker(food,poop)'
    for n in range(njobs):
        j = Process(target=worker, args=(food, poop))
        jobs.append(j)
        j.start()
    # wait for completion of all jobs:
    for j in jobs:
        j.join()
    # compile all fitness value calculated:
    cnt = 0
    sum = 0
    while True:
        try:
            i, f = poop.get_nowait()
            population[i].fitness = f
            cnt += 1
            sum += f
        except:
            break
    print("Average fitness for %i bugs: %.3f" % (cnt, sum/cnt))


# -------------------------------------------------------------------------------


def main(args):
    """
    By default, this reads its configuration from 'evolve.config'
    """
    config = 'evolve.config'
    genstart = ''
    for arg in args:
        if os.path.isdir(arg):
            genstart = arg + '/generation.txt'
        elif os.path.isfile(arg):
            if arg.endswith('.config'):
                config = arg
            elif arg.endswith('.txt'):
                genstart = arg
        else:
            sys.stderr.write("Error: unexpected argument `%s'" % arg)
            sys.exit(1)
    pam = {}
    with open(config, 'r') as f:
        for s in f:
            # Remove commented out lines
            if not s.strip().startswith('%'):
                exec(s, {}, pam)
    # print(pam)
    try:
        # extract some parameters out
        arena.template = pam.pop('config_file')
        arena.update_max_time()
        population_size = pam.pop('population_size')
        # sample_size = pam.pop('sample_size')
        generation_max = pam.pop('generation_max')
        njobs = pam.pop('njobs')
        FIT_DIFF_MAX = pam.pop('FIT_DIFF_MAX')
        FIT_MAX = pam.pop('FIT_MAX')
        elitism = pam.pop('elitism')
        crossover = pam.pop('crossover')
        mutation = pam.pop('mutation')
        mutation_rate = pam.pop('mutation_rate')
        resurrect = pam.pop('resurrect')
        n_plus = pam.pop('n_plus')
    except KeyError as e:
        sys.stderr.write(
            "Error: parameter %s must be defined in `%s'\n" % (str(e), config)
        )
        sys.exit()

    # Remaining parameters will be passed on to preconfig, generating multiple files
    for k, v in pam.items():
        print('Parameter: %s = ' % k, v)

    # initialize variables:
    heaven = {}
    generation = 0
    target = arena.load_target()
    code = load_genetics(arena.genetic_config)

    # check executable is there:
    if not os.access(arena.simex, os.X_OK):
        sys.stderr.write(
            "Error: could not find executable `%s`\n" % arena.simex
        )
        sys.exit(1)

    # initialize population:
    if os.path.isfile(genstart):
        population = load_generation(genstart, code)
        print("loaded %i genomes from %s" % (len(population), genstart))
    else:
        population = []
        for i in range(population_size):
            g = code.random_genome()
            population.append(Creature(code, g, i))
            # print(g)
        print("initialized from %i random genomes" % len(population))
    fitness_old = population[0].fitness

    # start evolution
    while generation < generation_max:
        """
        now we create configs and run simulations
        """
        genpath = "gen%04i" % generation + "/"
        mkdir(genpath)
        # copy template and genetics file:
        shutil.copyfile(arena.template, genpath + f'/{arena.template}')
        shutil.copyfile(arena.genetic_config, genpath+'/genetics.config')
        # prepare population:
        for i in population:
            i.path = genpath + i.path
            revive(heaven, i, resurrect)
            i.target = target
            mkdir(i.path)
            # calculate parameter values, and append global values from 'pam':
            val = i.values()
            for k, v in pam.items():
                val[k] = v
            # generate config files in bug's directory:
            preconfig.parse(arena.template, val, 1, i.path)
        # optional verification:
        # report_duplicates(population)

        # Calculate fitness values...
        if njobs > 1:
            # ...in parallel on multiple threads:
            run_parallel(population, njobs)
        else:
            # ...sequentially:
            for i in population:
                i.fitness = run_simulation(i)

        # sort population by fitness value:
        population = sorted(population, key=lambda x: -x.fitness)
        # best is now first in list:
        best = population[0]

        # we are done with the current generation
        save_generation(genpath+"generation.txt", population)
        print("Best %s: %f:\t" % (best.path, best.fitness), end="")
        print(tidy_dictionary(best.values()))

        # save tested genomes:
        save_dictionary(genpath+"heaven.txt", heaven)
        print(genpath+"heaven.txt has %i souls\n" % len(heaven))
        print("CPU %8.1f " % resource.getrusage(
            resource.RUSAGE_CHILDREN).ru_utime)

        """
            Stopping Conditions
        """
        # Check for convergence, given the best fitness reached in this generation
        if abs(best.fitness - fitness_old) < FIT_DIFF_MAX:
            print("FIT_DIFF_MAX is achieved")
            break
        if best.fitness > FIT_MAX:
            print("FIT_MAX is exceeded")
            break

        """
            Creation of a new generation
        """
        # Otherwise generate new offsprings for new generation
        fitness_old = best.fitness
        #elders = [ i for i in population if i.fitness > 0 ]
        elders = population
        population = []
        generation += 1
        Creature.counter = 0

        S, P, Q = partition(len(elders), elitism, crossover, mutation)
        #print("elitism, crossover, mutation = ", S, P, Q)

        """
            Elitism: transfer to the next generation without change
        """
        for n, i in enumerate(elders[:S]):
            population.append(i.update_path())
            #print(" %s  %s  fitness %10.4f" %(i.path, str(i), i.fitness))

        """
            Crossover: mating of two parents to produce one child
        """
        n_minus = 2.0 - n_plus
        # selection of parent's indices:
        parents = fitness_rank_selection(len(elders), 2*P, n_plus, n_minus)
        # check that we have at least 2 different possible parents:
        if len(set(parents)) > 1:
            print("crossover from %i pairs:" % len(parents), end='')
            for i in range(P):
                # parents is a list of indices:
                d = parents[2*i]
                m = parents[2*i+1]
                # dad and mom should be different:
                while d == m:
                    d = random.choice(parents)
                    m = random.choice(parents)
                print(" %i:%i" % (min(d, m), max(d, m)), end='')
                dad = elders[d]
                mom = elders[m]
                #child = dad.mate_uniform(mom)
                child = dad.mate_crossover(mom, 1)
                population.append(Creature(dad.code, child))
            print("")

        """
            Mutation: apply mutation rate to the rest of the population
        """
        parents = fitness_rank_selection(len(elders), Q, n_plus, n_minus)
        #parents = fitness_proportional_selection(elders, Q)
        if parents:
            print("mutations for %i individuals:" % len(parents), parents)
            for i in parents:
                # parents is a list of indices:
                parent = elders[i]
                child = parent.mutate(mutation_rate)
                population.append(Creature(parent.code, child))

        """
           complete population with random genomes
        """
        cnt = population_size - len(population)
        if cnt > 0:
            print("adding %i random genomes" % cnt)
            for i in range(cnt):
                g = code.random_genome()
                population.append(Creature(code, g))

    """
        Print final results at termination
    """
    print("Best fitness: %f" % (population[0].fitness))
    print(population[0].values())


# ------------------------------------------------------------------------

if __name__ == "__main__":
    if len(sys.argv) > 2:
        print(__doc__)
    elif len(sys.argv) == 2 and sys.argv[1] == 'help':
        print(__doc__)
    else:
        main(sys.argv[1:])
