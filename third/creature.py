# Python module that contains classes and functions needed to run a genetic algorithm.
# Maud Formanek, 2020, Francois Nedelec October 2021
# Copyright Sainsbury Laboratory, Cambridge University, UK

try:
    import sys
    import math
    import random
    import bitarray
    from genetics import LocusSet
except ImportError as e:
    sys.stderr.write("Error loading module: %s\n" % str(e))
    sys.exit()


class Creature(object):
    """ 
    Class representing an individual for the genetic algorithm
    It is defined by its genome (a bitvector) encoding parameter values
    The composition of this genome is defined by the 'code'
    """
    counter = 0

    def __init__(self, cod: LocusSet, gen: bitarray.bitarray,
                 path='', fit=math.nan):
        """ create Creature """
        if not isinstance(gen, bitarray.bitarray):
            gen = bitarray.bitarray(gen)
        if len(gen) != cod.nbits():
            sys.stderr.write(
                "Error: Genome needs to be of length %d!\n" % cod.nbits()
            )
            sys.exit()
        self.code = cod
        self.genome = gen
        self.fitness = fit
        if not path:
            self.update_path()
        elif isinstance(path, int):
            self.path = '%04i' % path
        else:
            self.path = path

    def update_path(self):
        self.path = '%04i' % Creature.counter
        Creature.counter += 1
        return self

    def sequence(self):
        """ Return a string containing ‘0’s and ‘1’s, representing the genome """
        return self.genome.to01()
        # return self.genome.unpack(b'-', b'X').decode()

    def __str__(self):
        """ return string representation of bit genomes"""
        return self.code.genome_to_string(self.genome)
        return self.sequence()

    def values(self):
        """ return dictionary of parameter values corresponding to genome"""
        return self.code.genome_to_values(self.genome)

    def mate_uniform(self, other):
        """ 
        Perform mating and produce new offspring via uniform crossover
        """
        L = len(self.genome)
        mut = bitarray.util.urandom(L)  # random bits
        return (self.genome & (~mut)) | (other.genome & mut)

    def mate_crossover(self, other, cnt):
        """ 
        Perform mating and produce new offspring via n crossover
        """
        L = len(self.genome)
        pos = sorted(random.sample(range(0, L-1), 2*cnt))
        mut = bitarray.util.zeros(L)
        for i in range(cnt):
            A = pos[2*i]
            B = pos[2*i+1]
            mut[A:B] = 1
        return (self.genome & (~mut)) | (other.genome & mut)

    def mutate(self, rate):
        """
            Mutate all bits with given probability
        """
        L = len(self.genome)
        mut = bitarray.bitarray([random.random() < rate for i in range(L)])
        return self.genome ^ mut
