# Python module that contains the parameter class needed to run a genetic algorithm.
# Maud Formanek, 2020, Francois Nedelec October 2021 -- Jan 2022
# Copyright Sainsbury Laboratory, Cambridge University, UK

try:
    import sys
    import os
    import bitarray
    import bitarray.util
except ImportError as e:
    sys.stderr.write("Error loading module: %s\n" % str(e))
    sys.exit()

# -------------------------------------------------------------------------------

err = sys.stderr


class Locus(object):
    """
        Defines a particular parameter to be optimized by the genetic algorithm. 
        Members include the parameter name, its range (lowerbound, upperbound)
        and the length (in bits) of its associated gene (bitlength)
    """

    def __init__(self, name, low, up, nb):
        self.name = name
        self.lowerbound = low
        self.upperbound = up
        self.scale = (up - low) / float(max(1, 2**nb-1))
        self.nbits = nb

    def map_to_bits(self, val):  # not sure this works or is needed
        """
            return bitvector corresponding to given value
        """
        v = (val - self.lowerbound) / self.scale
        bits = bitarray.util.int2ba(round(v), length=self.nbits)
        if len(bits) > self.nbits:
            err.write("Error: %s bitarray overflow %i > (length = %i)\n" %
                      (self.name, len(bits), self.nbits))
            sys.exit()
        return bits

    def map_from_bits(self, bits):
        """
            return value corresponding to given bitvector:
                lowerbound + value * range / (2**nbits-1)
            where `value` is the unsigned integer corresponding to the bitvector
        """
        if self.nbits > 0:
            v = bitarray.util.ba2int(bits)
            return self.lowerbound + v * self.scale
        return self.lowerbound


# -------------------------------------------------------------------------------

class LocusSet(object):
    """ A chromosomes is a list of Locuses """

    def __init__(self):
        self.Locuses = []

    def add(self, name, lower_bound, upper_bound, bitlength):
        """ 
        add a new parameter
        """
        P = Locus(name, lower_bound, upper_bound, bitlength)
        if not isinstance(P, Locus):
            err.write("Error: added parameter ", P,
                      "is not of class 'Locus'\n")
            sys.exit()
        else:
            self.Locuses.append(P)

    def nbits(self):
        """length in bits"""
        s = 0
        for P in self.Locuses:
            s += P.nbits
        return s

    def read(self, filename):
        read_params = False
        vals = {}
        with open(filename, 'r') as f:
            for s in f:
                try:
                    # Remove commented out lines
                    if not s.strip().startswith('%'):
                        exec(s, {}, vals)
                except Exception as e:
                    sys.stderr.write("Error: in `%s': %s" % (s, str(e)))
        if not vals:
            sys.stderr.write(
                "Error: `%s` could not been read correctly!" % filename)
        for key, val in vals.items():
            if isinstance(val, float):
                self.add(key, val, val, 0)
            elif len(val) == 2:
                self.add(key, val[0][0], val[0][1], val[1])
            else:
                sys.stderr.write("Unexected genetics: `%s = %s`" %
                                 (key, str(val)))

    def print(self):
        for loc in self.Locuses:
            n = loc.nbits
            bits = bitarray.util.zeros(n)
            l = loc.map_from_bits(bits)
            if n > 0:
                u = loc.map_from_bits(~bits)
                print(" %20s in [ %f %f ] (%i bits)" %
                      (loc.name, l, u, loc.nbits))
            else:
                print(" %20s = %f" % (loc.name, l))

    def save(self, filename):
        """
            print the parameter names used in the simulation along with their lower and upper bounds
        """
        with open(filename, 'w') as f:
            for P in self.Locuses:
                f.write("%s %f %f\n" % (P.name, P.lowerbound, P.upperbound))

    def random_genome(self):
        """
        create random bitarray of appropriate length
        """
        return bitarray.util.urandom(self.nbits())

    def genome_to_values(self, genome):
        """
           return a dictionary containing all the numeric values
        """
        i = 0
        res = {}
        for loc in self.Locuses:
            e = i + loc.nbits
            bits = genome[i:e]
            val = loc.map_from_bits(bits)
            res[loc.name] = val
            i = e
        #print(genome, "--->", res);
        return res

    def genome_to_string(self, genome):
        """
           return a string with 0 & 1 separated by groups
        """
        i = 0
        res = ''
        for loc in self.Locuses:
            e = i + loc.nbits
            bits = genome[i:e]
            res += "'" + genome[i:e].to01()
            i = e
        return res[1:]

# -------------------------------------------------------------------------------


def load_genetics(filename) -> LocusSet:
    g = LocusSet()
    g.read(filename)
    g.print()
    return g
