#!/usr/bin/env python

"""
Based on screen.py (Copyright F. J. Nedelec, 22.3.2019)
"""

try:
    import os
    import sys
    import subprocess
    import traceback
    import numpy as np
    from typing import List
except ImportError:
    sys.stderr.write("could not load essential python modules\n")
    sys.exit()

try:
    import preconfig
except ImportError:
    sys.stderr.write("could not load `preconfig'\n")
    sys.exit()

out = sys.stdout  # open("screen.txt", 'w')
err = sys.stderr


def job(execut, config, values, repeat):
    res = []
    # Vary parameters and generate files in current folder:
    confs = preconfig.Preconfig().parse(config, values, repeat, '')
    # Run simulation:
    for conf in confs:
        sub = subprocess.Popen([execut, '-', conf], stdout=subprocess.PIPE)
        # Get results from standard output:
        for data in sub.stdout:
            line = data.decode("utf-8")
            res.append(line)
        sub.stdout.close()
    return res


def executable(arg):
    return os.path.isfile(arg) and os.access(arg, os.X_OK)


def data_to_file(res: List[str], file='output.txt'):
    with open(file, 'w') as writer:
        for i, res_index in enumerate(range(3, len(res), 6)):
            dna1 = res[res_index]
            dna2 = res[res_index + 1]
            dna1_coords = '(' + ', '.join(dna1.split()[2:]) + ')'
            dna2_coords = '(' + ', '.join(dna2.split()[2:]) + ')'

            if dna1[1] == '2':
                dna1_coords, dna2_coords = dna2_coords, dna1_coords

            writer.write(f'------------ Run {i} ------------\n')
            writer.write(f'DNA 1: {dna1_coords}\t\tDNA 2: {dna2_coords}\n\n')


def get_part_score(res: List[str], file='output.txt'):
    part_scores = []

    for i in range(3, len(res), 6):
        dna1 = res[i]
        dna2 = res[i+1]
        # print(dna1, dna2)
        dna1_x = float(dna1.split()[2])
        dna2_x = float(dna2.split()[2])

        # TODO edge case x = 0?
        score_this_time = (-1 * np.sign(dna1_x) * np.sign(dna2_x) + 1) / 2
        part_scores.append(score_this_time)

    part_score = np.mean(part_scores)
    with open(file, 'a') as writer:
        writer.write(f'With a sample size of {len(res) / 2}, the '
                     f'mean partitioning score is\n{part_score}')

    return part_score


def run_simul(config, repeat):
    execut = os.path.abspath('sim')

    if not executable(execut):
        err.write("Error: executable `%s' could not be found\n" % execut)
        sys.exit()

    # gather data
    res = job(execut, config, {}, repeat)

    # write data to file
    data_to_file(res)

    # analyse data to get partition score
    part_score = get_part_score(res)
    print(f'Partitioning score was {part_score}')


if __name__ == "__main__":
    if len(sys.argv) == 1 or (len(sys.argv) == 2 and sys.argv[1] == 'help'):
        print(__doc__)
    elif len(sys.argv) == 2:
        try:
            run_simul('config.cym.tpl', int(sys.argv[1]))
        except ValueError as ex:
            traceback.print_exc()
