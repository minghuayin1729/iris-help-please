# Handy tools for a genetic algorithm.
# Maud Formanek, 2021, FJ Nedelec, 2021--2022, Copyright Cambridge University

try:
    import os
    import sys
    import shutil
    import re
    import subprocess
    import random
except ImportError as e:
    sys.stderr.write("Error loading modul: %s\n" % str(e))
    sys.exit()

# name to store output of simulations
results_file = 'all_results.txt'

# -------------------------------------------------------------------------------


def fitness_rank_selection(sup, cnt, n_plus, n_minus):
    """
        Selection of parents based on rank
        Returns 'cnt' integers in [0, sup-1]
    """
    if sup < 2:
        return []
    sel = []
    sum = 0
    beta = [0] * sup
    alpha = (n_minus - n_plus) / (sup - 1)
    for j in range(sup):
        sum += n_plus + alpha * j
        beta[j] = sum
    #print("rank_selection ", beta);
    # print(f'{sup = }, {sum = }')
    for _ in range(cnt):
        t = random.random() * sum
        s = 0
        for j in range(sup):
            if t < beta[j]:
                s = j
                break
        sel.append(s)
    return sel


def fitness_proportional_selection(pop, cnt):
    """
        Selection of parents based on relative fitness value
        Returns 'cnt' integers in [0, len(pop)-1]
    """
    sel = []
    sum = 0
    beta = [0] * len(pop)
    for i, p in enumerate(pop):
        sum += max(p.fitness, 0)
        beta[i] = sum
    #print("fitness_selection ", beta);
    for _ in range(cnt):
        t = random.random() * sum
        s = 0
        for j, b in enumerate(beta):
            if t < b:
                s = j
                break
        sel.append(s)
    return sel


def partition(TOT, pA, pB, pC):
    """Return 3 random integers with sum = TOT, according to probabilities"""
    if pA + pB + pC != 1:
        sys.stderr.write("Error: partition probabilities must add up to 1!")
        sys.exit()
    A = 0
    B = 0
    C = 0
    for _ in range(TOT):
        x = random.random()
        if x < pA:
            A += 1
        elif x < pA+pB:
            B += 1
        else:
            C += 1
    return A, B, C


def report_duplicates(pop):
    """ find duplicales in 'pop' """
    seen = set()
    for i in pop:
        g = i.sequence()
        if g in seen:
            print("duplicate genome " + g)
        else:
            seen.add(g)

# -------------------------------------------------------------------------------


def move_files(old, new):
    """
        move files between generations
    """
    try:
        shutil.move(old, new)
        #print(old, " ---move---> ", new)
    except Exception as e:
        print("move_files failed: " + str(e))
        pass


def copy_files(old, new):
    """
        copy files between generations
    """
    #shutil.copytree(old[:-1], self.path[:-1])
    try:
        shutil.copytree(old, new, dirs_exist_ok=True)
        #print(old, " ---copy---> ", new)
    except Exception as e:
        print("copy_files failed: " + str(e))
        pass

# -------------------------------------------------------------------------------


def mkdir(path):
    """ Make directory if it does not exist"""
    if not os.path.exists(path):
        os.mkdir(path)


def save_dictionary(file, dic):
    """write dictionary from file"""
    with open(file, 'w') as f:
        for k, v in dic.items():
            f.write("%s %s\n" % (k, str(v)))


def load_dictionary(file):
    """read values from file, returning a dictionary"""
    res = {}
    f = open(file, "r")
    for s in f:
        if not s or s[0] == '%' or s[0] == '#':
            continue
        k, v = s.split()
        res[k] = v
    f.close()
    return res


def tidy_dictionary(dic):
    """return string representing dictionary's values"""
    res = ''
    for k, v in dic.items():
        res += ", %s %.4f" % (k, v)
    return res[2:]


def load_values(file, unwanted):
    """load parameter values as a dictionary from file"""
    res = {}
    with open(file, 'r') as f:
        for s in f:
            exec(s, {}, res)
    for u in unwanted:
        if u in res:
            res.pop(u)
    return res

# -------------------------------------------------------------------------------


def save_results(name, res, txt=''):
    """ add results to file """
    with open(name, 'a') as f:
        if txt:
            f.write(txt)
        for x in res:
            f.write(x)
        f.write('\n\n')


def load_results(name):
    """ return list of results, separated by generations """
    out = []
    res = ''
    try:
        f = open(name, 'r')
        for s in f:
            if re.match('%gen[0-9]{4}', s):  # s.startswith('%gen[0-9]{4}'):
                if res:
                    out.append(res)
                res = ''
            else:
                res += s
        f.close()
        #print("loaded %i lines of old data"%len(res))
    except FileNotFoundError:
        pass
    if res:
        out.append(res)
    return out

# -------------------------------------------------------------------------------


def run_one(simex, conf):
    """
        Run simulation in current working directory and return its output
    """
    try:
        # simulation started as a subprocess
        sub = subprocess.Popen([simex, '-', conf], stdout=subprocess.PIPE)
        # get output from simulation
        out, err = sub.communicate()
        if err:
            sys.stderr.write("Subprocess failed in %s: %s\n" %
                             (os.getcwd(), err))
            sys.exit()
        return out.decode()
    except Exception as e:
        sys.stderr.write("Failed: %s\n" % repr(e))
    return ''


def run_many(simex, root, files):
    """
        Run all simulations sequentially in current directory
        It is assumed that current directory is set to 'root' already
    """
    all = []
    tmp = []
    for conf in files:
        res = run_one(simex, conf)
        if res:
            all.append(res)
            #print("  %s/%s : %i bytes" %(root, conf, len(res)));
            tmp.append(conf)
        else:
            sys.stderr.write("%s/%s : no output!\n" % (root, conf))
    # keep the first config file:
    # if len(tmp) > 0:
    #     tmp.pop(0)
    # load data from previous generations:
    old = load_results(results_file)
    if all:
        # save fresh data to file:
        save_results(results_file, all, '%'+root+'\n')
        # delete temporary files:
        for f in tmp:
            os.remove(f)
    else:
        sys.stderr.write("Error: no new results in `%s`\n" % root)
    return all, old


def run_many_separate(simex, root, files):
    """
        Runs all simulations sequentially in separate working directory
        It is assumed that current directory is set to 'root' already
    """
    all = []
    tmp = []
    """ run each simulation in a sub-directory """
    cwd = os.getcwd()
    for i, f in enumerate(files):
        sub = "run%04i" % i
        os.mkdir(sub)
        [conf, ext] = os.path.splitext(f)
        conf = 'config'+ext
        os.rename(f, os.path.join(sub, conf))
        os.chdir(sub)
        res = run_one(simex, conf)
        if res:
            all.append(res)
            #save_results('results.txt', res)
            #print("  %s/%s : %i bytes" %(root, sub, len(res)));
            tmp.append(sub)
        else:
            sys.stderr.write("%s/%s : no output!\n" % (root, sub))
        os.chdir(cwd)
    # load data from previous generations:
    old = load_results(results_file)
    if all:
        # save fresh data to file:
        save_results(results_file, all, '%'+root+'\n')
        # delete temporary directories:
        for p in tmp:
            shutil.rmtree(p)
    else:
        sys.stderr.write("Error: runs in `%s` all failed!\n" % root)
        # grab all data into a single file:
        #sys = os.system("cd %s && cat run??/results.txt > all_results.txt"%root)
        #res = load_results(results_file)
    return all, old
