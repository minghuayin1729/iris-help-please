# Provides methods for the artificial evolution
# Francois Nedelec January 2022
# Copyright Sainsbury Laboratory, Cambridge University, UK

try:
    import os
    import sys
    import re
    import numpy as np
    from typing import List
except ImportError as e:
    sys.stderr.write("Error loading module: %s\n" % str(e))
    sys.exit()

spatial = True

# simulation executable must be in current working directory:
simex = os.path.abspath('sim_spatial') if spatial else os.path.abspath('sim')
template = 'config.cym.tpl'
genetic_config = 'genetics.config'
max_time = 100  # units: s


# -------------------------------------------------------------------------------


def load_target():
    """read histogram data from file"""
    filename = 'target.txt'
    return 0


def update_max_time():
    global max_time

    with open(template) as f:
        for line in f:
            if line.strip().startswith('time_step'):
                time_step = float(line.split()[2])
            elif line.strip().startswith('run'):
                max_time = float(line.split()[1]) * time_step
                break


# -------------------------------------------------------------------------------


def calculate_fitness(data: List[str], target):
    """
    Calculate fitness expressing how close `data` is to `target`
    Higher fitness is better
    """
    # print(f'\n***{data[:3] = }***\n')

    data = data[0].split('\n')

    # Remove all the debugging print statements from the lines of output
    # data_copy = data.copy()
    # for line in data:
    #     if not line.strip().startswith('%'):
    #         data_copy.remove(line)
    # data = data_copy

    # print(f'\n***{data[:5] = }***\n')

    part_scores = []

    if spatial:
        for i, line in enumerate(data):
            if line.startswith('% report'):
                # Figure out if the partitioning was successful
                if i >= 2 and data[i-2].startswith('% time'):
                    # Partitioning successful
                    t = float(data[i-2].split()[2])
                    score_this_time = 1 - t/max_time
                    # Account for floating point errors
                    score_this_time = np.round(score_this_time, decimals=5)
                    print(f'With {max_time = }, {t = }, {score_this_time = }')
                else:
                    # Partitioning unsuccessful
                    score_this_time = 0

                part_scores.append(score_this_time)
    else:
        # Remove empty lines
        data_copy = data.copy()
        for line in data:
            if not line:
                data_copy.remove(line)
        data = data_copy

        for i in range(2, len(data), 4):
            dna1 = data[i]
            dna2 = data[i+1]
            # print(f'***DNA data ({i} of {len(data) // 6}): {dna1} {dna2}***')
            dna1_x = float(dna1.split()[2])
            dna2_x = float(dna2.split()[2])

            score_this_time = (-1 * np.sign(dna1_x) * np.sign(dna2_x) + 1) / 2
            # Account for floating point errors
            score_this_time = np.round(score_this_time, decimals=5)
            part_scores.append(score_this_time)

    part_score = np.round(np.mean(part_scores), decimals=5)
    return part_score
