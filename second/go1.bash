#!/usr/bin/bash
#! Which project should be charged:
#SBATCH -A NEDELEC-SL3-CPU
#! Which partition/cluster am I using?
#SBATCH -p skylake
#! How many nodes should be allocated? If not specified SLURM
#! assumes 1 node.
#SBATCH --nodes=1
#! How many tasks will there be in total? By default SLURM
#! will assume 1 task per node and 1 CPU per task.
#SBATCH --ntasks=32
#! How much memory in MB is required _per node_? Not setting this
#! as here will lead to a default amount per task.
#! Setting a larger amount per task increases the number of CPUs.
##SBATCH --mem=
#! How much wallclock time will be required?
#SBATCH --time=12:00:00
cd ~/rds/hpc-work/for_iris/second
source my_python/bin/activate

mkdir run1
rsync -a . run1/ --exclude="run*" --exclude="slurm*"

cd run1
time ./evolve.py >> output1.txt
