% Simulating partitioning
% Start with a cell that resembles S. pombe and experiment with
% different partitioning systems ('spindles')

set simul system
{
    time_step = 0.001
    viscosity = 0.1 % can specify different viscosities
    display = (label=(Partitioning simulation); size=1200, 700)
    income = 800000
}

set space cell
{
    shape = capsule
}

new cell
{
    radius = 0.4
    length = 2
}

%%% Microtubules
% Most of the default values come from hand_nucleate.cym
set fiber microtubule
{
    rigidity = 20
    segmentation = 0.2
    confine = inside, 100
    unit_energy_cost = 1625 % 13/0.008

    activity = dynamic % growing vs dynamic instability (could use = classic)
    unit_length = 0.008 % default
    growing_speed = [[growing_speed]] % default 0.2
    shrinking_speed = -0.5 % default
    hydrolysis_rate = 1.0 % default 1.0, catastrophe_rate approximately 1/8.3 s^-1
    growing_force = 1.7 % default
    total_polymer = 200 % may need to adjust this
    
    % max_length?
    % min_length?
}

%%% Motors
set hand kinesin
{
    binding_rate = 10
    binding_range = [[binding_range]] % default is 0.01 (in 2D anyway)
    unbinding_rate = 0.3
    unbinding_force = 2.5

    activity = move % crosslink
    unloaded_speed = 0.02 % mitotic kinesin very slow
    stall_force = 6
    hold_growing_end = 0 % change to 0?
    movement_cost = 125 % 1/0.008
    
    display = (color=red, red;)
}

set couple complex
{
    hand1 = kinesin
    hand2 = kinesin
    stiffness = 100
    diffusion = 0.05
    specificity = antiparallel
    length = 0.025
    fast_diffusion = 0
    synthesis_cost = 400
}

%%% DNA (plasmids)
set bead dna
{
    confine = inside, 100
    display = (color=green;)
}

%%% Nucleator
set hand nucleator
{
    unbinding = 0, inf % rate, force
    activity = nucleate
    nucleate = 1, microtubule, (length=0.01; plus_end=grow;)
    % rate, name of filament, new microtubule
    display = (color=gray;)
}

set single grower
{
    hand = nucleator
    stiffness = 1000
}

%%% Rescuer
set hand binder
{
    binding_rate = 10
    binding_range = [[binding_range_r]]
    unbinding_rate = 2
    unbinding_force = 3
    activity = rescue
    rescue_prob = [[rescue_prob]]
}

set single rescuer
{
    hand = binder
    stiffness = 100
    diffusion = 1
}

%%% Create objects
new dna
{
    radius = 0.1
    attach = [[int(growers_per_dna)]] grower
    position = -0.5 0 0
}

new dna
{
    radius = 0.1
    attach = [[int(growers_per_dna)]] grower
    position = -0.3 0 0
}

% TODO: does the C++ code do what we think?
% new [[int(num_complex)]] complex

new event
{
    activity = (new 1 complex)
    rate = [[motor_syn_rate]]
}

new event
{
    activity = (new 1 rescuer)
    rate = [[rescuer_syn_rate]]
}

run 100000 system
{
    nb_frames = 10000
}

report dna:position *
restart 39
